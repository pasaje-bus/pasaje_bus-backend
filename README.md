# Pasaje bus backend

Esta aplicación es el backend de este [repositorio](https://gitlab.com/pasaje-bus/pasaje-bus_frontend/-/tree/main).

## Requerimientos del sistema

Por ahora, la API sólo funciona mediante entorno local, por lo que para ejecutarla debe considerar tener instalado lo siguiente:

- Gitlab [https://about.gitlab.com/install/](https://about.gitlab.com/install/)
- Python 3.7 (o superior) [https://www.python.org/downloads/](https://www.python.org/downloads/)
- Django 2.1.15 (o superior) [https://www.djangoproject.com/download/](https://www.djangoproject.com/download/)
- Django Rest Framework 3.11.0 (o superior) [https://www.django-rest-framework.org/](https://www.django-rest-framework.org/)
- Postgres [https://www.postgresql.org/download/](https://www.postgresql.org/download/)
- psycopg2 2.8.5 (o superior) [https://pypi.org/project/psycopg2/](https://pypi.org/project/psycopg2/)
- django-cors-headers 3.2.1 (o superior) (Correr en terminal: pip install django-cors-headers (si usa python 3, hacer: pip3 install django-cors-headers))

## Clonar el repositorio

`https://gitlab.com/pasaje-bus/pasaje_bus-backend.git`

## Correr aplicación

````
cd pasaje_bus-backend
python3 manage.py runserver
````

## Consideraciones

- No se implementó acceso por usuario
- El modelo implementado es el que se encuentra en el archivo "Modelo de datos-pasajebus.png"
- La aplicación backend supone que el frontend se encuentra disponible en la dirección http://localhost:8080/
- En caso de que no fuera así, es necesario modificar el archivo "settings.py", ubicado en la carpeta "backend"
- Al final del archivo agregar:
````
CORS_ORIGIN_ALLOW_ALL = False
CORS_ORIGIN_WHITELIST = (
    'http://localhost:8080',
)
````

