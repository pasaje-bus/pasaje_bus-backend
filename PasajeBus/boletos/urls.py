from django.urls import path
# from boletos.views import TrayectoCreate, TrayectoDetail, TrayectoList, TrayectoDelete, TrayectoUpdate
from boletos import views

urlpatterns = [
    #urls para trayecto
    path('trayecto/list', views.TrayectoList.as_view(), name="list"),
    path('trayecto/create', views.TrayectoCreate.as_view(), name="create"),
    path('trayecto/detail/<str:pk>/', views.TrayectoDetail.as_view(), name="detail"),
    path('trayecto/update/<str:pk>/', views.TrayectoUpdate.as_view(), name="update"),
    path('trayecto/delete/<str:pk>/', views.TrayectoDelete.as_view(), name="delete"),
    
    # urls para buses
    path('buses/list', views.BusesList.as_view(), name="list"),
    path('buses/create', views.BusesCreate.as_view(), name="create"),
    path('buses/detail/<str:pk>/', views.BusesDetail.as_view(), name="detail"),
    path('buses/update/<str:pk>/', views.BusesUpdate.as_view(), name="update"),
    path('buses/delete/<str:pk>/', views.BusesDelete.as_view(), name="delete"),
    
    #urls para choferes
    path('choferes/list', views.ChoferesList.as_view(), name="list"),
    path('choferes/create', views.ChoferesCreate.as_view(), name="create"),
    path('choferes/detail/<str:pk>/', views.ChoferesDetail.as_view(), name="detail"),
    path('choferes/update/<str:pk>/', views.ChoferesUpdate.as_view(), name="update"),
    path('choferes/delete/<str:pk>/', views.ChoferesDelete.as_view(), name="delete"),
    
    #urls para pasajeros
    path('pasajeros/list', views.PasajerosList.as_view(), name="list"),
    path('pasajeros/create', views.PasajerosCreate.as_view(), name="create"),
    path('pasajeros/detail/<str:pk>/', views.PasajerosDetail.as_view(), name="detail"),
    path('pasajeros/update/<str:pk>/', views.PasajerosUpdate.as_view(), name="update"),
    path('pasajeros/delete/<str:pk>/', views.PasajerosDelete.as_view(), name="delete"), 

    #urls para boleto
    path('boleto/list', views.BoletoList.as_view(), name="list"),
    path('boleto/create', views.BoletoCreate.as_view(), name="create"),
    path('boleto/detail/<str:pk>/', views.BoletoDetail.as_view(), name="detail"),
    path('boleto/update/<str:pk>/', views.BoletoUpdate.as_view(), name="update"),
    path('boleto/delete/<str:pk>/', views.BoletoDelete.as_view(), name="delete"), 

    #urls para asiento
    path('asientos/list', views.AsientosList.as_view(), name="list"),
    path('asientos/create', views.AsientosCreate.as_view(), name="create"),
    path('asientos/detail/<str:pk>/', views.AsientosDetail.as_view(), name="detail"),
    path('asientos/update/<str:pk>/', views.AsientosUpdate.as_view(), name="update"),
    path('asientos/delete/<str:pk>/', views.AsientosDelete.as_view(), name="delete"), 
]