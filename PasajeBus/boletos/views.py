
from boletos.models import Asientos, Boleto, Buses, Choferes, Pasajeros, Trayecto
from rest_framework import generics
from boletos.serializers import AsientosSerializer, BoletoSerializer, BusesSerializer, ChoferesSerializer, BoletoSerializer,PasajerosSerializer, TrayectoSerializer

# trayecto view
class TrayectoCreate(generics.CreateAPIView):
    # para crear un nuevo trayecto
    queryset = Trayecto.objects.all()
    serializer_class = TrayectoSerializer

class TrayectoList(generics.ListAPIView):
    # para leer todos los trayectos
    queryset = Trayecto.objects.all()
    serializer_class = TrayectoSerializer

class TrayectoDetail(generics.RetrieveAPIView):
    # para consultar por un trayecto en particular por su id
    queryset = Trayecto.objects.all()
    serializer_class = TrayectoSerializer

class TrayectoUpdate(generics.RetrieveUpdateAPIView):
    # para actualizar un trayecto
    queryset = Trayecto.objects.all()
    serializer_class = TrayectoSerializer

class TrayectoDelete(generics.RetrieveDestroyAPIView):
    # para eliminar un trayecto
    queryset = Trayecto.objects.all()
    serializer_class = TrayectoSerializer

# buses view
class BusesCreate(generics.CreateAPIView):
    # para crear un nuevo bus
    queryset = Buses.objects.all()
    serializer_class = BusesSerializer

class BusesList(generics.ListAPIView):
    # para leer todos los Buses
    queryset = Buses.objects.all()
    serializer_class = BusesSerializer

class BusesDetail(generics.RetrieveAPIView):
    # para consultar por un bus en particular por su id
    queryset = Buses.objects.all()
    serializer_class = BusesSerializer

class BusesUpdate(generics.RetrieveUpdateAPIView):
    # para actualizar un bus
    queryset = Buses.objects.all()
    serializer_class = BusesSerializer

class BusesDelete(generics.RetrieveDestroyAPIView):
    # para eliminar un bus
    queryset = Buses.objects.all()
    serializer_class = BusesSerializer

# choferes view
class ChoferesCreate(generics.CreateAPIView):
    # para crear un nuevo chofer
    queryset = Choferes.objects.all()
    serializer_class = ChoferesSerializer

class ChoferesList(generics.ListAPIView):
    # para leer todos los Choferes
    queryset = Choferes.objects.all()
    serializer_class = ChoferesSerializer

class ChoferesDetail(generics.RetrieveAPIView):
    # para consultar por un chofer en particular por su id
    queryset = Choferes.objects.all()
    serializer_class = ChoferesSerializer

class ChoferesUpdate(generics.RetrieveUpdateAPIView):
    # para actualizar un Chofer
    queryset = Choferes.objects.all()
    serializer_class = ChoferesSerializer

class ChoferesDelete(generics.RetrieveDestroyAPIView):
    # para eliminar un Chofer
    queryset = Choferes.objects.all()
    serializer_class = ChoferesSerializer

# pasajeros view
class PasajerosCreate(generics.CreateAPIView):
    # para crear un nuevo pasajero
    queryset = Pasajeros.objects.all()
    serializer_class = PasajerosSerializer

class PasajerosList(generics.ListAPIView):
    # para leer todos los Pasajeros
    queryset = Pasajeros.objects.all()
    serializer_class = PasajerosSerializer

class PasajerosDetail(generics.RetrieveAPIView):
    # para consultar por un pasajero en particular por su id
    queryset = Pasajeros.objects.all()
    serializer_class = PasajerosSerializer

class PasajerosUpdate(generics.RetrieveUpdateAPIView):
    # para actualizar un Pasajero
    queryset = Pasajeros.objects.all()
    serializer_class = PasajerosSerializer

class PasajerosDelete(generics.RetrieveDestroyAPIView):
    # para eliminar un Pasajeros
    queryset = Pasajeros.objects.all()
    serializer_class = PasajerosSerializer

# boleto view
class BoletoCreate(generics.CreateAPIView):
    # para crear un nuevo boleto
    queryset = Boleto.objects.all()
    serializer_class = BoletoSerializer

class BoletoList(generics.ListAPIView):
    # para leer todos los boletos
    queryset = Boleto.objects.all()
    serializer_class = BoletoSerializer

class BoletoDetail(generics.RetrieveAPIView):
    # para consultar por un Boleto en particular por su id
    queryset = Boleto.objects.all()
    serializer_class = BoletoSerializer

class BoletoUpdate(generics.RetrieveUpdateAPIView):
    # para actualizar un Boleto
    queryset = Boleto.objects.all()
    serializer_class = BoletoSerializer

class BoletoDelete(generics.RetrieveDestroyAPIView):
    # para eliminar un Boleto
    queryset = Boleto.objects.all()
    serializer_class = BoletoSerializer

# asientos view
class AsientosCreate(generics.CreateAPIView):
    # para crear un nuevo asiento
    queryset = Asientos.objects.all()
    serializer_class = AsientosSerializer

class AsientosList(generics.ListAPIView):
    # para leer todos los asientos
    queryset = Asientos.objects.all()
    serializer_class = AsientosSerializer

class AsientosDetail(generics.RetrieveAPIView):
    # para consultar por un Asiento en particular por su id
    queryset = Asientos.objects.all()
    serializer_class = AsientosSerializer

class AsientosUpdate(generics.RetrieveUpdateAPIView):
    # para actualizar un Asiento
    queryset = Asientos.objects.all()
    serializer_class = AsientosSerializer

class AsientosDelete(generics.RetrieveDestroyAPIView):
    # para eliminar un Asiento
    queryset = Asientos.objects.all()
    serializer_class = AsientosSerializer


