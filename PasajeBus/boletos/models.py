from django.db.models.aggregates import Avg
from django.db import models
from django.db.models.deletion import CASCADE
from django.db.models.fields import AutoField
from django.core.validators import MaxValueValidator, MinValueValidator

class Trayecto(models.Model):
    id_trayecto = models.AutoField(primary_key=True)
    origen = models.CharField(max_length=45, blank=False, default='')
    destino = models.CharField(max_length=45, blank=False, default='')
    horario_salida = models.DateTimeField()
    id_bus = models.ForeignKey(
        'Buses',
        on_delete=models.CASCADE,
    )
    id_chofer = models.ForeignKey(
        'Choferes',
        on_delete=models.CASCADE
    )
    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=[
                    'origen', 
                    'destino',
                    'id_bus',
                    'id_chofer'], 
                name='unique Trayecto')
        ]

class Buses(models.Model):
    id_bus = models.AutoField(primary_key=True)
    patente = models.CharField(max_length=10, blank=False, default='')

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['patente'],
                name='unique buses'
            )
        ]

class Choferes(models.Model):
    id_chofer = models.AutoField(primary_key=True)
    nombre_chofer = models.CharField(max_length=45, blank=False, default='')
    rut_chofer = models.CharField(max_length=10, blank=False, default='')

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['nombre_chofer', 'rut_chofer'], name='unique choferes')
        ]

class Asientos(models.Model):
    opciones_estado = (
        ('e', 'Enabled'),
        ('d', 'Disabled')
    )
    id_asiento = AutoField(primary_key=True)
    numero_asiento = models.IntegerField(
        validators=[
            MaxValueValidator(10),
            MinValueValidator(1)
        ])
    id_trayecto = models.ForeignKey(
        'Trayecto',
        on_delete=models.CASCADE
    )
    estado = models.CharField(max_length=1, choices=opciones_estado, null=False)
    
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['id_trayecto', 'numero_asiento'], name='unique asientos')
        ]
    
    def ocupado(self, id_trayecto):
        return Asientos.objects.filter(id_trayecto=id_trayecto, numero_asiento=self).count() > 0

class Boleto(models.Model):
    id_boleto = models.AutoField(primary_key=True)
    id_trayecto = models.ForeignKey(
        'Trayecto',
        on_delete=models.CASCADE
    )
    numero_asiento = models.ForeignKey(
        'Asientos',
        on_delete=models.CASCADE
    )
    id_pasajero = models.ForeignKey(
        'Pasajeros',
        on_delete=models.CASCADE
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['id_trayecto', 'numero_asiento', 'id_pasajero'], name='unique boleto')
        ]

class Pasajeros(models.Model):
    id_pasajero = AutoField(primary_key=True)
    rut_pasajero = models.IntegerField(blank=True, null=True)
    nombre_pasajero = models.CharField(max_length=45, blank=False, null=True)
    telefono_pasajero = models.IntegerField(blank=True, null=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['rut_pasajero', 'nombre_pasajero', 'telefono_pasajero'], name='unique pasajeros')
        ]





