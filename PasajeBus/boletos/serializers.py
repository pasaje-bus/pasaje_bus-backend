from rest_framework import serializers
from boletos.models import Asientos, Boleto, Buses, Choferes, Pasajeros, Trayecto

class TrayectoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Trayecto
        fields = '__all__'

class BusesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Buses
        fields = '__all__'

class ChoferesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Choferes
        fields = '__all__'

class AsientosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Asientos
        fields = '__all__'

class BoletoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Boleto
        fields = '__all__'

class PasajerosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pasajeros
        fields = '__all__'

class TrayectoPromedioSerializer(serializers.ModelSerializer):
    num_horarios = serializers.SerializerMethodField()
    num_pasajeros = serializers.SerializerMethodField()
    promedio_pasajeros = serializers.SerializerMethodField()

    def get_num_horarios(self, obj):
        return obj.trayecto_horario_salida.all().count()

    def get_num_pasajeros(self, obj):
        return obj.pasajeros_id_pasajero.all().aggregates(id_boleto=Count('id_boleto')).get('id_boleto')
    
    def get_promedio_pasajeros(self, obj):
        promedio = Asientos.objects.all().aggregate(Avg('numero_asiento'))
        if promedio is None:
            return 0
        return promedio

class CapacidadBusSerializer(serializers.ModelSerializer):
    bus = serializers.CharField(read_only=True, source='buses.patente')
    chofer = serializers.CharField(read_only=True, source='choferes.name')
    origen = serializers.CharField(read_only=True, source='trayecto.origen')
    destino = serializers.CharField(read_only=True, source='trayecto.destino')
    num_asientos_bus = serializers.SerializerMethodField()
    num_asientos_ocupados = serializers.SerializerMethodField()
    porcentje_ocupado = serializers.SerializerMethodField()

    class Meta:
        model = Trayecto
        fields = ('id_trayecto', 'origen', 'destino', 'horario_salida',
            'bus', 'chofer', 'num_asientos_bus', 'num_asientos_ocupados',
            'porcentje_ocupado')

    def get_porcentaje_ocupado(self, obj):
        return obj.percentage
    
    def get_num_asientos_bus(self, obj):
        return obj.asiento_id_trayecto.count()

    def get_num_asientos_ocupados(self, obj):
        return obj.boleto.id_pasajero.count()